#!/usr/bin/env bash

#=============================#
#===== GENERAL CONSTANTS =====#
#=============================#
VERSION="0.1.0"

RE_IS_INT='^[0-9]+$'

SUPPORTED_EXTS=("cbz", "cbr", "pdf")

#============================#
#===== COLORS CONSTANTS =====#
#============================#
CLR_ESC="\033["

CLR_RESET="\e[0m"
CLR_RED="\e[0;91m"
CLR_GREEN="\e[0;92m"

#============================#
#===== GLOBAL VARIABLES =====#
#============================#

# General variables
FILENAME="$(basename $0)"
DIRECTORY="."
MODE="" # No default value

# Rename variables
NAME=""
PREVIEW=0
AUTOMATIC=0
STARTING_VOLUME=1
ZERO_PADDING=1

# Convert variable
FILE=""
KEEP_ORIGINAL=0
OUTPUT_DIR=""

#===============================#
#===== Display the version =====#
#===============================#
function display_version {
    echo ${VERSION}
}

#============================#
#===== Display the help =====#
#============================#
function usage {
    echo -ne "Usage: \n"
    echo -ne "\t${FILENAME} <command> [<args>...]\n"
    echo -ne "\t${FILENAME} [options]\n"
    echo -ne "\n"

    echo -ne "Description: \n"
    echo -ne "\tSimple utility to work with mangas/comics files.\n"
    echo -ne "\n"

    echo -ne "Options: \n"
    # -d, --dir
    echo -ne "\t-d, --dir"
    echo -ne "\tDirectory of the mangas\n"
    # -h, --help
    echo -ne "\t-h, --help"
    echo -ne "\tDisplay this message\n"
    # -v, --version
    echo -ne "\t-v, --version"
    echo -ne "\tDisplay version"
    echo -ne "\n"


    echo -ne "Commands: \n"
    # comet
    echo -ne "\tcomet"
    echo -ne "\tWork with Comet metadata\n"
    # convert
    echo -ne "\tconvert"
    echo -ne "\tConvert the mangas in the directory to CBZ. (input can be "
    echo -ne "PDF/CBR)\n"
    # rename
    echo -ne "\trename"
    echo -ne "\tRename the files in the directory\n"
}

function comet_usage {
    echo "Not implemented."
    exit 0
}

function convert_usage {
    echo "Not implemented."
    exit 0
}

function rename_usage {
    echo -ne "Rename mangas files in a directory following some customizable "
    echo -ne "rules.\n"
    echo -ne "\n"

    echo -ne "Usage: \n"
    echo -ne "\t${FILENAME} rename [options]\n"
    echo -ne "\n"

    echo -ne "rename options: \n"
    # --automatic
    echo -ne "\t--automatic "
    echo -ne "\t\tRename the file without confirmation. ⚠️ Rename cannot be "
    echo -ne "undone! ⚠️\n"
    # -n, --name
    echo -ne "\t-n, --name <arg>  "
    echo -ne "\tName of the manga, if not provided, the name will contain only "
    echo -ne "the volume number.\n"
    # --preview
    echo -ne "\t--preview"
    echo -ne "\t\tShow the result of the renaming without performing it or "
    echo -ne "asking for it.\n"
    # --starting-volume
    echo -ne "\t--starting-volume <arg>"
    echo -ne "\tVolume number where the count should start. (default: 1)\n"
    # --zero-padding
    echo -ne "\t--zero-padding <arg>"
    echo -ne "\tEnable the padding of the volume number with 0 to equalize the "
    echo -ne "width.\n"
    echo -ne "\t\t\t\tUse 0 to disable it and add +1 to add more 0 to the "
    echo -ne "volume number. (default: 1)\n"
    echo -ne "\n"

    echo -ne "Common options: \n"
    echo -ne "\t-d, --dir \tDirectory of the mangas\n"
    echo -ne "\t-h, --help \tDisplay this message\n"
}

#=================================================#
#===== Functinos used to parse the arguments =====#
#=================================================#

function comet_args {
    echo "Not implemented."
    exit 0
}

function convert_args {
    while [ $# -gt 0 ]; do
        case $1 in
            "-d" | "--dir")
                if [ $# -gt 1 ] && [ "$2" != "" ]; then
                    shift
                    if [ -d "$1" ]; then
                        DIRECTORY="$1"
                    else
                        echo "Error: convert: '$1' is not a valid directory."
                        exit 1
                    fi
                else
                    echo "Error: convert: $1 need an argument."
                    exit 1
                fi
                ;;
            "-f" | "--file")
                if [ $# -gt 1 ] && [ "$2" != "" ]; then
                    shift
                    if [ -f "$1" ]; then
                        FILE="$1"
                        DIRECTORY="$(dirname -- '$1')"
                    else
                        echo "Error: convert: '$1' is not a valid file."
                        exit 1
                    fi
                else
                    echo "Error: convert: $1 need an argument."
                    exit 1
                fi
                ;;
            "-k" | "--keep-original")
                KEEP_ORIGINAL=1
                ;;
            "-o" | "--output-dir")
                if [ $# -gt 1 ] && [ "$2" != "" ]; then
                    shift
                    if [ -d "$1" ];then
                        OUTPUT_DIR="$1"
                    else
                        echo "Error: convert: '$1' is not a valid directory."
                        exit 1
                    fi
                else
                    echo "Error: convert $1 need an argument."
                    exit 1
                fi
                ;;
            "")
                ;;
            *)
                echo "Error: convert: Unknown argument '$(basename -- ${1})'."
                exit 1
                ;;
        esac
        shift
    done
}

function rename_args {
    while [ $# -gt 0 ]; do
        case $1 in
            "-h" | "--help")
                rename_usage
                exit 0
                ;;
            "-d" | "--dir")
                if [ $# -gt 1 ] && [ "$2" != "" ]; then
                    shift
                    if [ -d "$1" ]; then
                        DIRECTORY="$1"
                    else
                        echo "Error: rename: '$1' is not a valid directory."
                        exit 1
                    fi
                else
                    echo "Error: rename: $1 need an argument."
                    exit 1
                fi
                ;;
            "--automatic")
                AUTOMATIC=1
                ;;
            "--preview")
                PREVIEW=1
                ;;
            "-n" | "--name")
                if [ $# -gt 1 ] && [ "$2" != "" ]; then
                    shift
                    NAME="$1"
                else
                    echo "Error: rename: $1 need an argument."
                    exit 1
                fi
                ;;
            "--starting-volume")
                if [ $# -gt 1 ] && [ "$2" != "" ]; then
                    shift
                    if [[ "$1" =~ $RE_IS_INT ]]; then
                        STARTING_VOLUME=$1
                    else
                        echo -ne "Error: rename: "
                        echo "--starting-volume must be an integer >= 0."
                        exit 1
                    fi
                else
                    echo "Error: rename: --starting-volume need an argument."
                    exit 1
                fi
                ;;
            "--zero-padding")
                if [ $# -gt 1 ] && [ "$2" != "" ]; then
                    shift
                    if [[ "$1" =~ $RE_IS_INT ]]; then
                        ZERO_PADDING=$1
                    else
                        echo -ne "Error: rename: "
                        echo "--zero-padding must be an integer >= 0."
                        exit 1
                    fi
                else
                    echo "Error: rename: --zero-padding need an argument."
                    exit 1
                fi
                ;;
            "")
                ;;
            *)
                echo "Error: rename: Unknown argument '$(basename -- ${1})'."
                exit 1
                ;;
        esac
        shift
    done
}

function args {
    if [ $# -eq 0 ]; then
        usage
        exit 1
    fi

    while [ $# -gt 0 ]; do
        case "$1" in
            "-h" | "--help")
                usage
                exit 0
                ;;
            "-d" | "--dir")
                if [ $# -gt 1 ] && [ "$2" != "" ]; then
                    shift
                    if [ -d "$1" ]; then
                        DIRECTORY="$1"
                    else
                        echo "Error: '$1' is not a valid directory."
                        exit 1
                    fi
                else
                    echo "Error: $1 need an argument."
                    exit 1
                fi
                ;;
            "-v" | "--version")
                display_version
                exit 0
                ;;
            "comet")
                MODE="$1"
                ;;
            "convert")
                MODE="$1"
                # Number of argument after convert
                x=$(( $# - ($#-1) ))
                shift

                convert_args "${@:$x}"
                break
                ;;
            "rename")
                MODE="$1"
                # Number of argument after rename
                x=$(( $# - ($#-1) ))
                shift

                rename_args "${@:$x}"
                break
                ;;
            "")
                ;;
            *)
                echo "Error: Unknown argument $(basename -- ${1})"
                exit 1
                ;;
        esac
        shift
    done
}

#====================================#
#===== RENAME COMMAND FUNCTION ======#
#====================================#
function rename {
    cd "${DIRECTORY}"

    SUPPORTED_EXTS=("cbz", "cbr", "pdf")
    mangas=()

    for file in *; do
        file_ext=${file##*.}
        case "${SUPPORTED_EXTS[@]}" in *"${file_ext}"*) mangas+=("$file") ;; esac
    done

    nbr_mangas=${#mangas[@]}

    if [ $nbr_mangas -eq 0 ]; then
        echo "No file with the supported format (${SUPPORTED_EXTS[@]}) found."
        echo "Stopping here."
        exit 0
    fi

    seq_arg='-w'
    if [ $ZERO_PADDING -eq 0 ]; then
        seq_arg=''
    fi

    new_num=($(seq $seq_arg $STARTING_VOLUME $(($STARTING_VOLUME + $nbr_mangas))))
    counter=0

    for manga in "${mangas[@]}"; do
        manga_ext=${manga##*.}

        zero_padding=$(seq -s '+' $ZERO_PADDING | sed 's/[0-9]//g')
        zero_padding=$(echo $zero_padding | sed 's/+/0/g')

        new_name="${zero_padding}${new_num[$counter]}.${manga_ext}"

        if [ "$NAME" != '' ]; then
            new_name="${NAME} ${new_name}"
        fi

        counter=$(($counter+1))

        echo "${manga} -> ${new_name}"

        rename=""
        if [ $AUTOMATIC -eq 0 ] && [ $PREVIEW -eq 0 ]; then
            read -p "Do you want to rename? [Y/n] " rename
        fi

        if [ $AUTOMATIC -eq 1 ] || [ "${rename^^}" == "Y" ]; then
            mv "${manga}" "${new_name}"
        fi
    done

    if [ $PREVIEW -ne 1 ]; then
        echo "Renaming done."
    fi
}

#=====================================#
#===== CONVERT COMMAND FUNCTIONS =====#
#=====================================#
function package_cbz {
    echo -ne "ℹ️ convert: Packaging CBZ..."

    tmp_folder="$2"
    cd "${tmp_folder}"

    filepath="$1"
    filename="$(basename -- "$filepath")"
    archive_name="${filename%.*}.cbz"
    zip -q -r "${archive_name}" *

    cd - > /dev/null 2>&1

    if [ "$OUTPUT_DIR" == "" ]; then
        OUTPUT_DIR="$(dirname -- "$filepath")"
    fi

    if [ ! -f "${OUTPUT_DIR}/${archive_name}" ]; then
        mv "${tmp_folder}/${archive_name}" "${OUTPUT_DIR}"
        echo -ne "\r                            \r"
    else
        echo -ne "\n"
        echo "⚠️ convert: '${archive_name}' is already in the folder."
        echo "What do you want to do?"
        echo -e "\t[1] Overwrite the file."
        echo -e "\t[2] Use a new name."
        echo -e "\t[3] Nothing."

        regex_answer='^[1-3]{1}$'
        answer=""
        until [[ "$answer" =~ $regex_answer ]]; do
            echo -ne "➡️ "
            read answer

            answer_size_as_space="$(seq -s ' ' $((${#answer}+1)) | sed 's/[0-9]//g')"
            echo -ne "\r"
            echo -ne "\e[1A" # Go to the last line
            echo -ne "\r   ${answer_size_as_space}\r"
        done

        echo -ne "\n"

        case $answer in
            1)
                mv "${tmp_folder}/${archive_name}" "${OUTPUT_DIR}"
                echo "ℹ️ convert: Packaging done."
                ;;
            2)
                new_file_name="${archive_name}"
                until [ ! -f "${new_file_name}" ]; do
                    echo "Enter a name that is not in the folder."
                    read -p "➡️ " new_file_name
                    new_file_name="${new_file_name}.cbz"
                done

                pushd "${OUTPUT_DIR}" > /dev/null 2>&1
                mv "${tmp_folder}/${archive_name}" "${new_file_name}"
                popd > /dev/null 2>&1

                echo "ℹ️ convert: Packaging done."
                ;;
            3)
                # Do nothing
                echo "ℹ️ convert: Skipping the file."
                ;;
            *)
                # Do nothing
                ;;
        esac
    fi
}

function extract_cbr {
    echo -ne "ℹ️ convert: Extracting CBR..."

    tmp_folder="$2"
    if [ ! -d "${tmp_folder}" ]; then
        mkdir "${tmp_folder}"
    fi

    unrar x -inul "$1" "${tmp_folder}"

    if [ $? -ne 0 ]; then
        return 1
    fi

    echo -ne "\r                             \r"

    return 0
}

function extract_pdf {
    echo "Not implemented."
}

function convert {
    cd "${DIRECTORY}"

    flag_cbr=0
    flag_pdf=0

    if [ "$FILE" != "" ]; then
        file_ext=${FILE##*.}
        case "${SUPPORTED_EXTS[@]}" in *"${file_ext}"*) to_convert=("$FILE") ;; esac

        case "${file_ext}" in
            "cbr")
                flag_cbr=1
                ;;
            "pdf")
                flag_pdf=1
                ;;
            *)
                ;;
        esac
    else
        to_convert=()
        for f in *; do
            file_ext=${f##*.}
            case "${SUPPORTED_EXTS[@]}" in *"${file_ext}"*) to_convert+=("$f") ;; esac

            case "${file_ext}" in
                "cbr")
                    flag_cbr=1
                    ;;
                "pdf")
                    flag_pdf=1
                    ;;
                *)
                    ;;
            esac
        done
    fi

    if [ ${#to_convert[@]} -eq 0 ]; then
        echo "No file with the supported format (${SUPPORTED_EXTS[@]}) found."
        echo "Stopping here."
        exit 0
    fi

    if [ ${flag_cbr} -eq 1 ] && [ ! -x $(command -v unrar) ]; then
        echo "Error: convert: 'unrar' is needed to convert 'cbr' files."
        exit 1
    fi

    if [ ${flag_pdf} -eq 1 ] && [ ! -x $(command -v convert) ]; then
        echo "Error: convert: 'convert' is needed to convert 'pdf' files."
        echo "'convert' come with Imagemagick package."
        exit 1
    fi

    # init temp folder
    tmp_dir=$(mktemp -d --tmpdir='/tmp' 'mangas.sh-XXXXX')

    for i in $(seq 0 $(( ${#to_convert[@]} - 1 ))); do
        file="${to_convert[i]}"
        file_ext=${file##*.}

        delete_original=0
        case "${file_ext}" in
            "cbr")
                # test if it's a RAR archive
                echo -ne "ℹ️ convert: Checking file..."
                unrar t "${file}" > /dev/null 2>&1
                is_rar=$?
                echo -ne "\r                            \r"

                if [ ${is_rar} -ne 0 ]; then
                    echo "⚠️ '${file}' is not a RAR archive."
                    zipinfo -h "${file}" > /dev/null 2>&1
                    if [ $? -eq 0 ]; then
                        echo "ℹ️ It's a ZIP archive. Updating extension to cbz."

                        mv "${file}" "${file%.*}.cbz"
                    else
                        echo "ℹ️ File skipped."
                    fi
                else
                    extract_cbr "${file}" "${tmp_dir}"

                    if [ $? -ne 0 ]; then
                        echo -n "⚠️ convert: can't extract files from '${file}'."
                        echo " Skipping it."
                        continue
                    fi

                    package_cbz "${file}" "${tmp_dir}"
                    # We should test if package_cbz ended well
                    delete_original=1
                    rm -rf ${tmp_dir}/*
                fi
                ;;
            "pdf")
                extract_pdf "${file}"
                ;;
            *)
                continue
                ;;
        esac

        if [ ${KEEP_ORIGINAL} -eq 0 ] && [ ${delete_original} -eq 1 ]; then
            # We are in the right folder so we can delete the file directly
            rm "${file}"
        fi
    done

    # delete temp folder
    rm -rf "${tmp_dir}"

    echo "☑️ convert: Convertion ended."
}

#===========================#
#===== COMET FUNCTIONS =====#
#===========================#
function comet {
    echo "Not yet implemented."
    exit 0
}

#=============================#
#===== Starting function =====#
#=============================#
function main {
    args "$@"

    case $MODE in
        "comet")
            ;;
        "convert")
            convert
            ;;
        "rename")
            rename
            ;;
        *)
            echo "Error: You need to pass a command."
            exit 1
            ;;
    esac

    exit 0
}

# Start the script
main "$@"